﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CreateImagePPM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CreateImagePPM.Tests
{
    [TestClass()]
    public class RayonTests
    {
        [TestMethod()]
        public void IntersectASphereTest()
        {
            Sphere sphere = new Sphere(_p: new Vector3(150, 150, 400), _radius: 150, _color: new Color(255, 255, 0));
            Sphere sphere2 = new Sphere(new Vector3(150, 150, 200), 100, new Color(200, 0, 255));
            Sphere sphere3 = new Sphere(new Vector3(150, 150, 80), 50, new Color(0, 120, 120));
            Assert.Fail();
        }
    }
}