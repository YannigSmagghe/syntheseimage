﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Numerics;
using System.Threading.Tasks;

namespace CreateImagePPM
{
    public class Sphere
    {
        public Vector3 p;
        public int radius;
        public Color color;
        public Light light;

        public Sphere(Vector3 _p, int _radius,Color _color)
        {
            p = _p;
            radius = _radius;
            color = _color;
        }
    }
}
