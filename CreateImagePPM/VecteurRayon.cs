﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateImagePPM
{
    class VecteurRayon
    {
        private int X;
        private int Y;
        private int Z;

        public VecteurRayon(int _x, int _y, int _z)
        {
            if (_x >= int.MinValue && _x <= int.MaxValue)
            {
                this.X = _x;
            }
            if (_y >= int.MinValue && _y <= int.MaxValue)
            {
                this.Y = _y;
            }
            if (_z >= int.MinValue && _z <= int.MaxValue)
            {
                this.Z = _z;
            }

        }

    }
}
