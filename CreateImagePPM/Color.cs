﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateImagePPM
{
    public class Color
    {
        public double r, g, b;

        public Color(double _r, double _g, double _b)
        {
            r = _r;
            g = _g;
            b = _b;
        }
    }
}
