﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CreateImagePPM
{
    class Camera
    {
        public Vector3 o;
        public int height, width;
        public Vector3 d;

        public Camera(Vector3 _origine, int _height, int _width, Vector3 _direction)
        {
            o = _origine;
            height = _height;
            width = _width;
            d = _direction;
        }
    }
}
