﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;


namespace CreateImagePPM
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "toto.ppm";
            //Image img = new Image(800, 600);
           

            // img.drawASphere(sphere);
            // Envoie d'un rayon de lumière

            //Rayon Lampe = new Rayon(new Vector3(20, 40, 1), new Vector3(10, 10, 0));
            //img.drawARayon(Lampe);

            //Rayon rayon = new Rayon(new Vector3(x, y, 0), new Vector3(x+1, y+1, 0));
            //img.drawIntersect(rayon, sphere);
            //List<Sphere> listSphere = new List<Sphere>();
            //listSphere.Add(sphere);
            //listSphere.Add(sphere2);
            Sphere sphere = new Sphere(_p: new Vector3(150, 150, 400), _radius: 150, _color: new Color(255, 255, 0));
            Sphere sphere2 = new Sphere(new Vector3(150, 150, 200), 100, new Color(200, 0, 255));
            Sphere sphere3 = new Sphere(new Vector3(150, 150, 80), 50, new Color(0, 120,120));
            Sphere sphere4 = new Sphere(new Vector3(20, 20,600), 300, new Color(169, 17,1));

            Scene scene1 = new Scene(_height: 800,_width: 600,_cam: new Camera(_origine: new Vector3(0, 0, 1), _height: 600, _width: 600, _direction: new Vector3(-1,-1,4))
                                    , _light: new Light(new Vector3(0, 300,-500)));
            scene1.spheres.Add(sphere);
            scene1.spheres.Add(sphere2);
            scene1.spheres.Add(sphere3);
            scene1.spheres.Add(sphere4);

            //Image img = scene1.DrawImg();


            //img.drawPixel(listSphere);



            // Ma sphère existe et on va essayer de la toucher avec mon rayon
            //img.drawIntersect(rayon, sphere);

            WritePPM(fileName, scene1.DrawImg());

            System.Diagnostics.Process.Start(@"D:\VisualStudio Workspace\CreateImagePPM\CreateImagePPM\bin\Debug\" + fileName);

            //Console.WriteLine("Done, press any key to exit.");
            //Console.ReadKey();
        }

        public static void WritePPM(string file, Image img)
        {
            //Use a streamwriter to write the text part of the encoding.
            var width = img.width;
            var height = img.height;
            var writer = new StreamWriter(file);
            writer.Write("P3" + "\n");
            writer.Write(width + " " + height + "\n");
            writer.Write("255" + "\n");


            for (int x = 0; x < height; x++)
            for (int y = 0; y < width; y++)
            {
                writer.Write(img.r[x, y]);
                writer.Write(" ");
                writer.Write(img.g[x, y]);
                writer.Write(" ");
                writer.Write(img.b[x, y]);
                writer.Write(" ");
            }
            writer.Close();
        }
    }

}
