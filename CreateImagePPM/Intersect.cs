﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace CreateImagePPM
{
    class Intersect
    {

        public void IntersectCalcul()
        {
            // a dist2(D); dist2 == Direction au carré
            // P et D = point et direction les deux sont des vecteurs
            // C centre de la sphère est vecteur aussi

            //B 2*(dot(P,D) - dot(C,D);
            float Rayon = 450;

            Vector3 Point = new Vector3(20, 30, 40);
            Vector3 Direction = new Vector3(45, 70, 80);
            Vector3 Centre = new Vector3(70, 50, 10);

            // Direction Vector
            Double A = dist2(Point);

            // autre calcul
            Double resultB = calculB(Point, Direction, Centre);

            //Autre calcul
            Double resultC = calculC(Point, Centre, Rayon);

            var delta = calculDelta(A, resultB, resultC);

            // Faire le calcul sur delta

            if (delta< 0)
            {
                // pas d'intersection
                Console.Error.WriteLine("null");
            }

            else if (delta >= 0)
            {
                double result = 0;
                result = (-resultB - (Math.Sqrt(delta)) / 2 * A);
                if (result >= 0)
                {
                    Console.Error.WriteLine("2 point et j'affiche le plus proche");
                    Console.Error.WriteLine(result);
                }
                else
                {
                    result = (-resultB + (Math.Sqrt(delta)) / 2 * A);
                    Console.Error.WriteLine("le poitn était dans le cercle");
                    Console.Error.WriteLine(result);

                }

            }

            Console.Error.WriteLine(A);
            Console.Error.WriteLine(resultB);
            Console.Error.WriteLine();
            Console.Error.WriteLine(delta);

            // C = dist2(C-Point) -  R²



            // Calcul Dot product


            // Test dotProduct
            var dotProduct = Vector3.Dot(Point, Centre);
            Console.Error.WriteLine(dotProduct);
            Console.ReadKey();
        }

        public static double calculDelta(double A, double B, double C)
        {
            double result = (B * B) - (4 * A * C);
            return result;
        }


        public static double calculC(Vector3 Centre, Vector3 Point, float Rayon)
        {
            double result = dist2(Centre - Point) - (Rayon * Rayon);
            return result;
        }

        //A
        public static double dist2(Vector3 direction)
        {
            Double lengthSqResult = direction.LengthSquared();
            return lengthSqResult;
        }

        // B
        public static double calculB(Vector3 Point, Vector3 C, Vector3 D)
        {

            double result = 2 * (Vector3.Dot(Point, D) - Vector3.Dot(C, D));

            return result;
        }

        public static double intersect()
        {
            double result = 0;

            return result;
        }
    }

    
}
