﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;


namespace CreateImagePPM
{
    class Image
    {
        public int height;
        public int width;
        public double[,] r;
        public double[,] g;
        public double[,] b;

        public Image(int _height, int _width)
        {
            height = _height;
            width = _width;
            r = new double[height, width];
            g = new double[height, width];
            b = new double[height, width];

            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    r[i, j] = 0;
                    g[i, j] = 0;
                    b[i, j] = 0;
                }
        }

        public void drawASphere(Sphere sphere)
        {

            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {

                    if (Math.Pow(i - sphere.p.X, 2) + Math.Pow(j - sphere.p.Y, 2) < Math.Pow(sphere.radius, 2))
                    {
                        r[i, j] = 255;
                        g[i, j] = 75;
                        b[i, j] = 75;
                    }
                }
        }
        public void SetPixel(int x, int y, int r, int g, int b)
        {
            this.r[x, y] = Clamp(r);
            this.g[x, y] = Clamp(g);
            this.b[x, y] = Clamp(b);
        }

        public double Clamp(double color)
        {
            if (color <= 0) color = 0;
            if (color >= 255) color = 255;
            color = color / 255;
            color = Math.Pow(color, 1/ 2.2);
            color = color * 255;   
            return color;
        }

        //public void drawPixel(List<Sphere> Listsphere)
        //{
        //    for (int iImg = 0; iImg < height; iImg++)
        //        for (int j = 0; j < width; j++)
        //        {
        //            //                                          ,distance l'axe Z,    direction
        //            Rayon rayon = new Rayon(new Vector3(iImg, j, 0), new Vector3(0, 0, 1));

        //            float shortestSphere = Int32.MaxValue;
        //            Console.Error.WriteLine();
        //            foreach (var sphere in Listsphere)
        //            {
        //                float impactSphere = rayon.IntersectASphere(sphere);
        //                Console.Error.WriteLine("impac {0}", shortestSphere);
        //                if (shortestSphere > impactSphere)
        //                {
        //                    shortestSphere = impactSphere;
        //                }
        //            }
                   
        //            if (shortestSphere != -1)
        //            {
        //                Console.Error.WriteLine("impac {0}", shortestSphere);
        //                int color = (int)shortestSphere;
        //                if (color > 255) color = 255;
        //                if (color < 0) color = 0;

        //                int color2 = color + 20;
        //                if (color2 > 255) color2 = color2;
        //                if (color2 < 0) color2 = color2;
        //                //Console.Error.WriteLine(color);

        //                r[iImg, j] = 255;
        //                g[iImg, j] = 255;
        //                b[iImg, j] = 255;
        //            }
        //        }
        //}

        public void ColorSphereWhite(Sphere sphere)
        {
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {

                    if (Math.Pow(i - sphere.p.X, 2) + Math.Pow(j - sphere.p.Y, 2) < Math.Pow(sphere.radius, 2))
                    {
                        r[i, j] = 35;
                        g[i, j] = 105;
                        b[i, j] = 225;
                    }
                }
        }
        private void ColorSphereBlue(Sphere sphere)
        {
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {

                    r[i, j] = 35;
                    g[i, j] = 105;
                    b[i, j] = 225;
                }
        }

        public void drawARayon(Rayon rayon)
        {
            // point départ
            r[(int)rayon.o.X, (int)rayon.o.Y] = 248;
            g[(int)rayon.o.X, (int)rayon.o.Y] = 255;
            b[(int)rayon.o.X, (int)rayon.o.Y] = 9;

            // fin rayon
            //r[(int)rayon.end.X, (int)rayon.end.Y] = 0;
            //g[(int)rayon.end.X, (int)rayon.end.Y] = 255;
            //b[(int)rayon.end.X, (int)rayon.end.Y] = 0;


            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    if (rayon.IsOnRayon(new Vector3(i, j, 50)))
                    {
                        Console.WriteLine("oui");
                        r[i, j] = 255;
                        g[i, j] = 13;
                        b[i, j] = 13;
                    }

                }
        }

        public void drawIntersect(Rayon rayon, Sphere sphere)
        {
            float x = rayon.IntersectASphere(sphere);
            if (x != -1)
            {
                Vector3 i = Vector3.Add(rayon.o, Vector3.Multiply(x, rayon.d));

                Console.WriteLine((int)i.X);
                Console.WriteLine(i.Y);
                Console.WriteLine("touché");
                //ColorSphereWhite(sphere);
                r[(int)i.X, (int)i.Y] = 0;
                g[(int)i.X, (int)i.Y] = 255;
                b[(int)i.X, (int)i.Y] = 255;
            }
            else
            {
                ColorSphereBlue(sphere);
            }

        }


    }
}
