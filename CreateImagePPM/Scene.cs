﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CreateImagePPM
{
    class Scene
    {
        public Camera cam;
        public Light light;
        public List<Sphere> spheres;
        public int height;
        public int width;

        public Scene(int _height, int _width, Camera _cam, Light _light)
        {
            height = _height;
            width = _width;
            cam = _cam;
            light = _light;
            spheres = new List<Sphere>();
        }

        public Image DrawImg()
        {
            Image img = new Image(cam.width, cam.height);
            for (int x = (int)cam.o.X; x < cam.o.X + cam.width; x++)
                for (int y = 0; y < cam.o.Y + cam.height; y++)
                {
                    Rayon cameraView = new Rayon(new Vector3(x, y, cam.o.Z), cam.d);
                    Sphere showThisSphere = null;
                    float firstItemHitDist = float.MaxValue;
                    float sphereDist = 0;
                    foreach (Sphere s in spheres)
                    {
                        sphereDist = cameraView.IntersectASphere(s);
                        if (sphereDist != -1 && firstItemHitDist > sphereDist)
                        {
                            firstItemHitDist = sphereDist;
                            showThisSphere = s;
                        }

                    }

              
                    // Lighting sphere & shadow
                    if (firstItemHitDist != float.MaxValue)
                    {
                        Vector3 pointOnSphere = Vector3.Add(new Vector3(x, y, cam.o.Z), Vector3.Multiply(firstItemHitDist, cam.d));

                        //On décale i un tout petit peu vers l'extérieur de la sphère pour être sur de pas être dans la sphère.
                        //On calcule le vecteur pointSphere->centreSphere, on le normalise et on l'inverse
                        // Inversé centre et point poru retirer le negate
                        Vector3 normaleSphere = Vector3.Normalize(Vector3.Subtract(pointOnSphere,showThisSphere.p));

                        pointOnSphere = Vector3.Add(pointOnSphere, normaleSphere *0.001f);
                        
                        Rayon sphereToLight = new Rayon(pointOnSphere, Vector3.Subtract(light.o, pointOnSphere));
                        bool seeTheLight = true;
                        foreach (Sphere s in spheres)
                        {
                            if (sphereToLight.IntersectASphere(s) != -1)
                            {
                                seeTheLight = false;
                                break;
                            }

                        }
                        double albedo = 0.30;
                        Surface surface1 = new Surface(albedo);
                        // Résultat de  formule est (Albedo*cosTheta) / Pi
                        double albedoOnSphere = surface1.DifuseLight(surface1.albedo, Vector3.Normalize(sphereToLight.d), normaleSphere);
                        // Facteur 1/D²
                        float epsilon = 0;
                        //pointOnSphere = Vector3.Add(pointOnSphere, sphereToLight.d * epsilon);
                        int powerOfLightValue = 500;
                        Vector3 powerOfLight = new Vector3(powerOfLightValue, powerOfLightValue, powerOfLightValue);
                        Vector3 distanceLight = surface1.DistanceLight(powerOfLight, pointOnSphere, light.o);
                        int intensity = 200;
                        double powerOfShadow = 0.5;
                        if (seeTheLight)
                        {
                            // La valeur 255 ici est complétement arbitraire, elle permet à partir de la valeur de mes calculs de donner une intensité plus ou moins forte
                            // on peut faire le test avec des lumière changeante et avec des valeurs autres que 255
                            img.SetPixel(x, y, 
                                (int)(showThisSphere.color.r * distanceLight.X * intensity * albedoOnSphere * intensity), 
                                (int)(showThisSphere.color.g * distanceLight.Y * intensity * albedoOnSphere * intensity),
                                (int)(showThisSphere.color.b * distanceLight.Z * intensity * albedoOnSphere * intensity));
                        }
                        else
                        {
                            img.SetPixel(x, y,
                                (int)(showThisSphere.color.r * powerOfShadow * distanceLight.X * intensity * albedoOnSphere * intensity),
                                (int)(showThisSphere.color.g * powerOfShadow * distanceLight.Y * intensity * albedoOnSphere * intensity),
                                (int)(showThisSphere.color.b * powerOfShadow * distanceLight.Z * intensity * albedoOnSphere * intensity));
                        }
                    }
                }
            return img;

        }
    }
}
