﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace CreateImagePPM
{
    public class Rayon
    {

        public Vector3 o;
        public Vector3 d;
        public Vector3 end;

        public Rayon(Vector3 _o, Vector3 _d)
        {
            o = _o;
            d = _d;
            end = Vector3.Add(o, d);
        }

        public bool IsOnRayon(Vector3 v)
        {
            Vector3 end = Vector3.Add(o, d);
            if (((v.X - o.X) / (end.X - o.X)) == ((v.Y - o.Y) / (end.Y - o.Y))
                && ((v.X - o.X) / (end.X - o.X)) == ((v.Z - o.Z) / (end.Z - o.Z)))
                return true;
            else
                return false;
        }


        public float IntersectASphere(Sphere sphere)
        //RETURN -1 IF NO INTERSECT
        {
            // equation qui sert à modeliser une sphere mathématiquement parlant
            float A = Vector3.Dot(d, d);
            float resultB = 2 * (Vector3.Dot(o, d) - Vector3.Dot(sphere.p, d));
            float c = Vector3.Dot(Vector3.Subtract(sphere.p, o), Vector3.Subtract(sphere.p, o)) - sphere.radius * sphere.radius;

            // Calcul pour savoir si j'ai hit la sphere compris entre compris entre l'origine et la hauteur de la sphère ou négatif si je ne hit pas
            float delta = (resultB * resultB) - (4 * A * c);

            if (delta >= 0)
            {
                // Si delta est positif c'est une racine (une solution possible) result1 et result2 sont les deux possiblitées, les deux bords de ma sphère que je 
                float result1 = (float)(-resultB + Math.Sqrt(delta)) / (2 * A);
                float result2 = (float)(-resultB - Math.Sqrt(delta)) / (2 * A);
                // On va chercher ici la valeur la plus petite car c'est celle qui est plus proche de mon rayon, on se fiche de ce qu'il y'a derrière
                // Mathématiquement parlant, result2 est une soustraction et a donc plus de chance d'être la plus petite valeur
                if (result2 >=  0)
                {
                    return result2;
                }
                else if (result1 >= 0)
                {
                    return result1;

                }
                // On n'a pu toucher la sphere mais derrière notre rayon
                else
                {
                    return -1;
                }
            }
            // on n'as pas touché la sphere
            else
            {
                return -1;
            }

        }

        
    }
}
